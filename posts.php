<header id="header" class="alt">
    <nav id="nav">
	<ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="posts.php">Posts</a></li>
	</ul>
    </nav>
</header>
<section>
    <h1>Uploader une image</h1>
    <form id ="formPost" action="Controller/postsController.php" method="POST" enctype="multipart/form-data">
        <input name="media[]" id ="media" type="file" accept="image/*" multiple>
        <br>
        <textarea name="comment" form="formPost" placeholder="Enter a comment here..."></textarea>
        <br>
        <input type="submit" name="submitPicture" id="submitPicture" value="Envoyer">
    </form>
</section>
<section>
    <h1>Uploader une vidéo</h1>
    <form id = "formPostVideo" action="Controller/postsController.php" method="POST" enctype="multipart/form-data">
        <input name="mediaVideo[]" id ="mediaVideo" type="file" accept="video/*" multiple>
        <br>
        <textarea name="comment" form="formPost" placeholder="Enter a comment here..."></textarea>
        <br>
        <input type="submit" name="submitVideo" id="submitVideo" value="Envoyer">
    </form>
</section>