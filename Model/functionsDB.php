<?php

/*
 *  M152 Blog
  Mugny Loïc
  January - March 2019
 */

include 'mySql.inc';
include '../Classes/Post.php';


function myDatabase() {
    static $dbc = null;

    if ($dbc == null) {
        try {
            $dbc = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
                PDO::ATTR_PERSISTENT => true));
            $dbc ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo 'Erreur : ' . $e->getMessage() . '<br />';
            echo 'N° : ' . $e->getCode();
            die('Could not connect to MySQL');
        }
    }
    return $dbc;
}

/**
 * Enregistre un utilisateur en base
 * @return boolean si tout est ok ou pas
 */
function insertPost($comment) {
    //Créer le string contenant l'insertion en base de l'utilisateur (cf Ressources)
    //Si l'exécution de la requête fonctionne
    //Début Si
    //Retourne vrai
    //Fin Si
    //Début Sinon
    //Retourne faux
    //Fin Sinon
    $connect = myDatabase();
    try {
        $connect->beginTransaction();
        $req = $connect->prepare('INSERT INTO `post`(`commentaire`) VALUES (:comment)');
        $req->bindParam(':comment', $comment, PDO::PARAM_STR);
        $req->execute();
        $return = $connect->lastInsertId();
        $connect->commit();
        RETURN $return;
    } catch (Exception $ex) {
        echo $ex ->getMessage();
        $connect->rollBack();
        return FALSE;
    }
}
function insertMedia($nameMedia, $typeMedia, $idPost) {
    //Créer le string contenant l'insertion en base de l'utilisateur (cf Ressources)
    //Si l'exécution de la requête fonctionne
    //Début Si
    //Retourne vrai
    //Fin Si
    //Début Sinon
    //Retourne faux
    //Fin Sinon
    try {
        myDatabase()->beginTransaction();
        $connect = myDatabase();
        $req = $connect->prepare('INSERT INTO `media`(`nomFichierMedia`, `typeMedia`, `idPost`) VALUES (:nameMedia, :typeMedia, :idPost)');
        $req->bindParam(':nameMedia', $nameMedia, PDO::PARAM_STR);
        $req->bindParam(':typeMedia', $typeMedia, PDO::PARAM_STR);
        $req->bindParam(':idPost', $idPost, PDO::PARAM_STR);
        $req->execute();
        myDatabase()->commit();
        RETURN TRUE;
    } catch (Exception $ex) {
        echo $ex ->getMessage();
        myDatabase()->rollBack();
        return FALSE;
    }
}

function getPosts(){
    try {
        $connect = myDatabase();
        $req = $connect->prepare("SELECT post.idPost, commentaire, datePoste, nomFichierMedia FROM post LEFT OUTER JOIN media ON post.idPost = media.idPost");
        $req->execute();
        $result = $req->fetchAll();
        $posts = [];
        foreach ($result as $post) 
        {
            $singlePost = new Post();
            $singlePost->id = $post['idPost'];
            $singlePost->comment = $post['commentaire'];
            $singlePost->nomMedia = $post['nomFichierMedia'];
            $singlePost->datePosted = $post['datePoste'];
            array_push($posts, $singlePost);
        }
    } catch (Exception $ex) {
        return FALSE;
    }
    return $posts;
}

function getMediaName($idPost) {
    try {
        $connect = getConnexion();
        $req = $connect->prepare("SELECT nomMedia FROM medias WHERE idPost = :idpost");
        $req->bindParam(':idpost', $idPost, PDO::PARAM_INT);
        $req->execute();
        $result = $req->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    } catch (PDOException $e) {
        return FALSE;
    }
}

function upload() {
    $medias = array("image", "video", "audio");
    $upload = false;
    if (!isset($_GET['idPost'])) {
        if (createPost($_POST['comment']) == true) {
            $idPost = getConnexion()->lastInsertId();
        }
    } else {
        $idPost = $_GET['idPost'];
    }
    $target_dir = "images/";
    foreach ($medias as $key) {
        if (isset($_FILES[$key]['name'])) {
            $nbFilesToUpload = count($_FILES[$key]['name']);
            for ($i = 0; $i < $nbFilesToUpload; $i++) {
                $name = basename($_FILES[$key]["name"][$i]);
                $name = bin2hex(random_bytes(32));
                $target_file = $target_dir . $name;
                $FileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
                if (in_array($FileType, array('png', 'jpg', 'bmp', 'mp4', 'mp3'))) {
                    if (move_uploaded_file($_FILES[$key]["tmp_name"][$i], $target_file)) {
                        if (insertMedia($idPost, $FileType, $name) == true) {
                            $upload = true;
                        }
                    } else {
                        return FALSE;
                    }
                }
            }
        }
    }
    if ($upload == true) {
        echo "Upload effectué avec succès";
    }
}

function deletePost($idPost) {
    try {
        $connect = getConnexion();
        $req = $connect->prepare("DELETE FROM posts WHERE idPost = :idpost");
        $req->bindParam(':idpost', $idPost, PDO::PARAM_INT);
        $req->execute();
        $req2 = $connect->prepare("DELETE FROM medias WHERE idPost = :idpost");
        $req2->bindParam(':idpost', $idPost, PDO::PARAM_INT);
        $req2->execute();
        return TRUE;
    } catch (PDOException $e) {
        return FALSE;
    }
}

function getMedias($idPost) {
    try {
        $connect = getConnexion();
        $req = $connect->prepare("SELECT * FROM medias WHERE idPost = :idpost");
        $req->bindParam(':idpost', $idPost, PDO::PARAM_INT);
        $req->execute();
        $result = $req->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    } catch (Exception $ex) {
        return FALSE;
    }
}


function getMediaName($idPost) {
    try {
        $connect = getConnexion();
        $req = $connect->prepare("SELECT nomMedia FROM medias WHERE idPost = :idpost");
        $req->bindParam(':idpost', $idPost, PDO::PARAM_INT);
        $req->execute();
        $result = $req->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    } catch (PDOException $e) {
        return FALSE;
    }
}
