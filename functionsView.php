<?php
/*
 *  M152 Blog
  Mugny Loïc
  January - March 2019
 */

require "../Model/functionsDB.php"

function showPosts($data) {
    foreach ($data as $value) {
        $date = new DateTime($value['datePost']);
        $result = $date->format('l, j F Y H:i');
        echo '<div class="blog-post">
            <form action="#" method="POST">
            <button type="submit" class="file-upload" name="deletePost" id="' . $value['idPost'] . '">Supprimer le post</button><br>
            <a href="editPost.php?idPost=' . $value['idPost'] . '" class="file-upload" name="editPost"">Modifier le post</a><br><br>
            <input type="hidden" value="' . $value['idPost'] . '" name="getIdPost">
            </input>            
            </form>
            <h2 class="date">' . $result . '</h2>
            <p class="blog-content">' . $value['commentaire'] . '</p>';
        showMedias(getMedias($value['idPost']), false);
        echo '</div>';
    }
}

function showMedias($data, $edit) {
    foreach ($data as $value) {
        $editButton = '<form action="#" method="POST">'
                . '<button type="submit" class="delete-file" name="deleteMedia">X</button>'
                . '<input type="hidden" value="' . $value['idMedia'] . '" name="getIdMedia">'
                . '<input type="hidden" value="' . $value['nomMedia'] . '" name="getNameMedia">
            <br></form>';
        if ($value['typeMedia'] == "mp4") {
            echo '<video controls autoplay loop class="media"> <source src="images/' . $value['nomMedia'] . '" type="video/mp4"></video>';
            if ($edit == true) {
                echo $editButton;
            }
        }
        if ($value['typeMedia'] == "mp3") {
            echo '<audio controls class="media"> <source src="images/' . $value['nomMedia'] . '" type="audio/mp3"></audio>';
            if ($edit == true) {
                echo $editButton;
            }
        }
        if ($value['typeMedia'] == "png" || $value['typeMedia'] == "jpg" || $value['typeMedia'] == "bmp") {
            echo '<img src="images/' . $value['nomMedia'] . '" class="media">';
            if ($edit == true) {
                echo $editButton;
            }
        }
    }
}