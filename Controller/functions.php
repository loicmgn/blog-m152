<?php
/*
 *  M152 Blog
  Mugny Loïc
  January - March 2019
 */

function uploadMedia($titles, $tmpNames) {
    if (is_array($titles)) {
        $result = [];
        for ($i = 0; $i < count($titles); $i++) {
            if (!uploadSingleMedia($titles[$i], $tmpNames[$i])) {
                return false;
            }
            array_push($result, $titles[$i]);
        }
    } else {
        if (!uploadSingleMedia($titles, $tmpNames)) {
            return false;
        } else {
            $result = $titles;
        }
    }
    return $result;
}

function uploadSingleMedia($title, $tmpName) {
    $cnt = 1;
    while (checkMediaExists($title)) {
        $title = substr($title, 0, -4) . '-' . $cnt . substr($title, -4);
        $cnt++;
    }
    $targetFile = 'media\\' . $title;
    $result = false;
    if (move_uploaded_file($tmpName, $targetFile)) {
        $result = $title;
    }
    return $result;
}